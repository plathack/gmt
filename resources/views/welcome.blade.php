<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" 
    integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
    <title>GMT</title>
</head>
<body>
    <div class="bg-light vh-100 vw-100">
            <div class="d-flex flex-column justify-content-center text-center bg-success p-2 text-white bg-opacity-25">
                <div class="header__title">GiveMeTask</div>
                <nav class="d-flex justify-content-around">
                    <div class="nav__elem"><a class="text-decoration-none" href="">Random task</a></div>
                    <div class="nav__elem"><a class="text-decoration-none" href="">Practice</a></div>
                    <div class="nav__elem"><a class="text-decoration-none" href="">Raiting</a></div>
                    @guest
                    <div class="nav__elem"><a class="text-decoration-none" href="{{ route('login') }}">Login</a></div>
                    @endguest
                    @auth
                    <div class="nav__elem"><a class="text-decoration-none" href="{{ route('logout') }}">Logout</a></div>
                    @endauth
                </nav>
            </div>
            <div class="content">
                
            </div>
            <footer class="">
                Thank you
            </footer>
    </div>
</body>
</html>