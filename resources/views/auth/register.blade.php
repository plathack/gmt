<form action="{{ route('registerPost') }}" method="post">
    @csrf
    <div>
        <input type="text" name="name" placeholder="name">
    </div>
    @error('name')
        {{ $message }}
    @enderror
    <br>
    <div>
        <input type="text" name="email" placeholder="email">
    </div>
    @error('email')
        {{ $message }}
    @enderror
    <br>
    <div>
        <input type="password" name="password" placeholder="password">
    </div>
    @error('password')
        {{ $message }}
    @enderror
    <br>
    <div>
        <input type="password" name="password_confirmation" placeholder="password confirmed">
    </div>
    @error('password_confirmation')
        {{ $message }}
    @enderror
    <br>
    <button>Register</button>
    <br>
    <a href="{{ route('login') }}">Есть аккаунт?</a>
</form>
