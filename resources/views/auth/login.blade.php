<form action="{{ route('loginPost') }}" method="post">
    
    <div>
        <input type="text" name="email" placeholder="email">
    </div>
    @error('email')
        {{$message}}
    @enderror
    <br>
    <div>
        <input type="password" name="password" placeholder="password">
    </div>
    @error('password')
        {{$message}}
    @enderror
    <br>
    <button>Login</button>
    <br>
    <a href="{{ route('register') }}">Нет аккаунта?</a>
</form>