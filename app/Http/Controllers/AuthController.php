<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function showLoginForm()
    {
        return view('auth.login');
    }

    public function showRegisterForm()
    {
        return view('auth.register');
    }

    public function login(Request $request)
    {
        $credentials = $request->validate([
            'email' => ['required', 'email', 'string'],
            'password' => ['required', 'string']
        ]);

        if (auth('web')->attempt($credentials))
        {
            return redirect('/');
        }

        return redirect(route('login'))->withErrors([
            'email' => 'Credentials wrong'
        ]);
    }

    public function register(Request $request)
    {
        $credentials = $request->validate([
            'name' => ['required', 'string'],
            'email' => ['required', 'email', 'string', 'unique:users'],
            'password' => ['required', 'confirmed', 'string']
        ]);

        $user = User::create([
            'name' => $credentials['name'],
            'email' => $credentials['email'],
            'password' => bcrypt($credentials['password'])
        ]);

        if ($user)
        {
            auth('web')->login($user);
            return redirect('/');
        }

        return redirect(route('register'));
    }

    public function logout()
    {
        auth('web')->logout();
        return redirect('/');
    }
}
